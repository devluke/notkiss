#!/bin/bash

read -p "==> Choose a name for your project: " name
echo "==> Choose a language for your project:"
echo "=> (1) Swift"
echo "=> (2) Python 3"
echo "=> (3) Python 2"
echo "=> (4) Objective-C"
echo "=> (5) C"
echo "=> (6) C++"

while :; do
	read -p "==> Language: " language

	if [ "$language" != 3 ]; then
		echo "==> That language is not yet supported."
	else
		break
	fi
done

template=python2

echo "==> Creating new project, because KISS sucks..."
cp -r "$NKISS/templates/$template" "$name"
