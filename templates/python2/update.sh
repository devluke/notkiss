#!/bin/bash

echo "==> Preparing..."
mkdir scripts
mv *.py scripts
rm -rf scripts/better_wallaby.py

echo "==> Updating project..."
cp $NKISS/Template/* .
mv scripts/* .
rm -rf scripts
