#!/bin/bash

host=root@192.168.125.1

echo "==> Preparing..."
mkdir run
cp *.py run

echo "==> Copying files..."
scp -r run $host:~/ > /dev/null

echo "==> Running main.py..."
echo
ssh $host 'bash -l -c "python ~/main.py && rm -rf ~/run"'

rm -rf run
