# NotKISS

Allows replacement of KISS IDE

## Install

### Automatic

Simply open a Terminal window and run this command:

`bash <(curl -s https://gitlab.com/devluke/notkiss/raw/master/install.sh)`

### Manual

To install, clone this repository to your home directory:

`cd ~/ && git clone https://gitlab.com/devluke/notkiss.git`

Then, open your shell's RC file and add this line to the end of the file:

`export NKISS=~/NotKISS`

Restart your Terminal and NotKISS should now work!

## Usage

Create a new project:

`$NKISS/new.sh`

Change to the newly created project's directory:

`cd <project name>`

The Python script you'll want to edit is `main.py`. When you're ready to run your script:

`./run.sh`

That's it, your script is now running!

## Updating

Update your main NotKISS directory:

`$NKISS/update.sh`

For each of your projects, change to its directory and run:

`./update.sh`

## Windows

Windows is not a \*nix operating system and therefore does not come with the correct type of shell. If you wish to use this tool on Windows, look [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to install a Linux subsystem on your Windows installation.

## Custom templates

If you wish to use a custom `main.py` template instead of the default, run this command:

`cp path/to/new.py $NKISS/Template/main.py`
