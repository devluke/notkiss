#!/bin/bash

if [ ! -n "$BASH" ]; then
	echo "==> You are not running this script with bash. Please run this script again with bash."
	exit 1
fi

git help &> /dev/null
echo "==> If you got a popup asking to install command line tools, install it."
read -p "==> Press enter/return once it's done installing or if you never got a popup... "

echo "==> Downloading NotKISS..."
git clone https://gitlab.com/devluke/notkiss.git ~/NotKISS &> /dev/null

echo "==> Installing NotKISS..."
echo "export NKISS=~/NotKISS" >> ~/.bash_profile

echo "==> Done, restart Terminal to finish the installation."
